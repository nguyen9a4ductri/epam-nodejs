var express = require('express');
var router = express.Router();
let fs = require('fs')
let path =require('path')
let filespath = './files/'
let supportFiles= ['.log', '.txt', '.json','.yaml', '.xml', '.js']

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/api/files', (req,res,next)=>{
  let params = req.body
  if(!params.filename){
    res.status(400).json({message: 'Please specify \'filename\' parameter'})
    return
  }
  if(!fs.existsSync(filespath))
    fs.mkdirSync(filespath)
  if(fs.existsSync(filespath+params.filename)){
    res.status(400).json({message: params.filename+ ' has already created'})
    return
  }
  if(!supportFiles.find(e=>path.extname(params.filename)===e)){
    res.status(400).json({message: 'This webapp doesn\'t support '+path.extname(params.filename)+' extension' })
    return
  }
  if(!params.content){
    res.status(400).json({message: 'Please specify \'content\' parameter'})
    return
  }

  fs.appendFileSync(filespath+params.filename,params.content,'utf-8')

  //external requirement for file password, using filename.pass
  if(params.password){
    fs.appendFileSync(filespath+params.filename+'.pass',params.password,'utf-8')
  }

  res.status(200).json({message: 'File created successfully'})
})

router.get('/api/files', (req,res,next)=>{
  fs.readdir(filespath,(err,files)=>{
    if(err){
      res.status(400).json({message: 'Client error'})
      return
    }else{
      res.status(200).json({
        message: 'Success',
        files: files.filter(e=>path.extname(e)!=='.pass')
      })
    }
  })
})

router.get('/api/files/:filename',(req,res,next)=>{
  let filename= req.params.filename
  fs.readFile(filespath+filename,'utf-8',(err,data)=>{
    if(err){
      res.status(400).json({message: 'No file with \''+ filename+'\' found'})
      return
    }else{
      if(fs.existsSync(filespath+filename+ '.pass')){
        if(!req.body?.password){
          res.status(400).json({message: 'Password is require to open this file'})
          return
        }
        else if(req.body.password!==fs.readFileSync(filespath+filename+'.pass','utf-8')){
            res.status(400).json({message: 'Please provide correct password for this file'})
            return
        }
      }
      let uploadedDate = fs.statSync(filespath+filename).birthtime
      let extension = path.extname(filename)

      res.status(200).json({
        message: "Success",
        filename,
        content: data,
        extension,
        uploadedDate
      })
    }
  })
})


//optional request body should have {password: optional, content}
router.put('/api/files/:filename',(req,res,next)=>{
  let filename=req.params.filename
  if(fs.existsSync(filespath+filename)){
    if(fs.existsSync(filespath+filename+'.pass')){
      if(!req.body?.password){
        res.status(400).json({message: 'Password is require to modify this file'})
        return
      }
      else if(req.body.password!==fs.readFileSync(filespath+filename+'.pass','utf-8')){
          res.status(400).json({message: 'Please provide correct password for this file'})
          return
      }
    }
    let content = req.body?.content
    if(!content){
      res.status(400).json({message: 'Content should be provided to make changes'})
      return
    }
    fs.writeFile(filespath+filename,content,'utf-8',(err)=>{
      if(err){
        res.status(400).json({message: 'There is a problem to modify this file'})
        return
      }
      res.status(200).json({message: "This file is modified successfully"})
    })
  }else
    res.status(400).json({message: filename + ' doesn\'t exist'})
})

router.delete('/api/files/:filename',(req,res,next)=>{
  let filename=req.params.filename
  if(fs.existsSync(filespath+filename)){
    if(fs.existsSync(filespath+filename+'.pass')){
      if(!req.body?.password){
        res.status(400).json({message: 'Password is require to delete this file'})
        return
      }
      else if(req.body?.password!==fs.readFileSync(filespath+filename+'.pass','utf-8')){
          res.status(400).json({message: 'Please provide correct password for this file'})
          return
      }
      fs.unlink(filespath+filename+'.pass',(err)=>{
        if(err){
          res.status(400).json({message: 'There is a problem when deleting this file'})
          return
        }
      })
    }

    fs.unlink(filespath+filename,(err)=>{
      if(err){
        res.status(400).json({message: 'There is a problem when deleting this file'})
        return
      }    
      res.status(200).json({message: "This file is deleted successfully"})
    })
  }else
    res.status(400).json({message: filename + ' doesn\'t exist'})
})

module.exports = router;
